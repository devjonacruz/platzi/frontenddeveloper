# ¿Qué son y para qué nos sirven HTML y CSS?

**HTML:** Es un lenguaje de marcado usado para decirle a tu navegador cómo estructurar las páginas web que visitas. No es un lenguaje de programación.

**CSS:** Es un lenguaje que nos permite crear páginas web con un diseño agradable para los usuarios. Tampoco es un lenguaje de programación.

# DOM, CSSOM, Render Tree y el proceso de renderizado de la Web

**DOM:** Document Object Model. Es una transformación del código HTML escrito por nosotros a objetos entendibles para el navegador.

**CSSOM:** así como el DOM para el HTML, EL CSSOM es una representación de objetos de nuestros estilos en CSS.

**Render Tree:** es la unión entre el DOM y el CSSOM para renderizar todo el código de nuestra página web.

Pasos que sigue el navegador para construir las páginas web:

1. Procesa el HTML para construir el DOM.
2. Procesa el CSS para construir el CSSOM.
3. El DOM se une con el CSSOM para crear el Render Tree.
4. Se aplican los estilos CSS en el Render Tree.
5. Se ““pintan”” los nodos en la pantalla para que los usuarios vean el contenido de la página web.

# 5 TIPS PARA APRENDER CSS

![5 tips](src_platzi/Infografia-Frontend-Javascript-986d1fea-9f09-4b8e-be0d-6d9f69ac75b8.jpg)

# Anatomía de un Elemento HTML: Atributos, Anidamiento y Elementos vacíos

Nuestros elementos HTML se componen de:

* Etiqueta de apertura: el nombre de nuestra etiqueta encerrado entre símbolos de mayor o menor. Por ejemplo: `<h1>`.
* Contenido: dentro de nuestras etiquetas podemos añadir texto u otros elementos HTML, lo que conocemos como anidamiento.
* Etiqueta de cierre: son casi iguales que las etiquetas de apertura, pero también necesitan un slash (/) antes del nombre de la etiqueta. Por ejemplo: `</h1>`.

Las etiquetas de apertura también pueden tener atributos. Los atributos nos permiten definir características especiales para nuestros elementos: `<etiqueta atributo=""valor del atributo"">`. Por ejemplo: `<h1 class=""saludo"">`.

También existen elementos vacíos. Estos elementos no tienen contenido ni etiqueta de cierre, solo etiqueta de apertura y atributos. Por ejemplo: `<img src=""puppy.png"" alt=""mi mascota"">`.

# Anatomía de un Documento HTML: DOCTYPE, html, head y body

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
  </head>
  <body></body>
</html>

```

# La importancia del código semántico

Es importante que como desarrolladores tengamos claro el significado de escribir código. Debes ser consciente de que la manera en la que codeas tenga sentido.

La semántica HTML no es más que darle sentido y estructura a lo que estas escribiendo. Muy importante para el navegador. No todos los elementos deberían ser un div.

# Tipos de errores en HTML, debugging y servicio de validación de etiquetas

**Errores sintácticos:** Son errores de escritura en el código y evitan que el programa funcione. Pueden ser errores de tipado.

**Errores lógicos:** En estos la sintaxis es correcta, pero el código no hace lo que debería. El programa funciona, pero de forma incorrecta.

# Anatomía de una declaración CSS: Selectores, Propiedades y Valores

Nuestros estilos con CSS se componen de:

* **Selector:** son la referencia a los elementos HTML que queremos estilizar. Los nombres de estas etiquetas van seguidas de una llave de apertura y otra de cierre ({}). Por ejemplo: h1 {}.
* **Propiedades:** son el tipo de estilo que queremos darle a nuestros elementos. Van seguidas de dos puntos (:). Las propiedades deben estar dentro de las llaves del selector que definimos anteriormente. Podemos escribir diferentes propiedades en un mismo selector. Por ejemplo: h1 { color: }.
* **Valores:** son el estilo que queremos que tomen nuestros elementos HTML con respecto a una propiedad. Van seguidas de un punto y coma (;). Por ejemplo: `h1 { color: red; }`.

```css
h1 {
  color: red;
}
```

# Tipos de selectores, pseudo-clases y pseudo-elementos

***(asterisco):** Es el selector universal. Las propiedades se aplicaran a todos los elementos de nuestro HTML.

**Tipo:** Son selectores que se aplican a cierto elemento HTML en específico. Las propiedades se aplicaran a la etiqueta que queremos, por ejemplo p, body, html, div, etc.

**Clase:** Si nuestras etiqueta de HTML tienen un atributo de class podemos usar ese valor o identificador para que los cambios en el CSS afecten únicamente a ese elemento.

**ID:** Es similar al anterior, si la etiqueta HTML tiene un ID podemos afectar solo ese elemento.

Las Pseudo-clases y Pseudo-elementos nos permiten ser aún más específicos con qué elemento o partes de nuestros elementos deben recibir los estilos.

Para usarlas debemos definir el selector base (por ejemplo, p) seguido de dos puntos y la pseudo-clase que queremos estilizar (por ejemplo: `p:first-child`). En el caso de los pseudo-elementos debemos usar el dos puntos 2 veces (`p::first-letter`).

```css
/* Asterisco (universal) */
* {
  margin: 0;
}

/* Tipo */
h1 {
  color: red;
}

/* Clase */
.saludo {
  font-size: 2em;
}

/* ID */
#id {
  border-radius: 20px;
}

/* Pseudo-clases */
p:first-child {
  color: white;
}

p:last-child {
  color: purple;
}

p:nth-child(2n) {
  color: red;
}
```

# Modelo de caja

Todos los elementos de HTML tienen un modelo de caja y esta compuesto por cuatro elementos: **contenido**, **padding**, **border**, **margin**.

# Valores relativos y absolutos

Los valores **absolutos** son, por ejemplo, centímetros, milímetros, pixeles y pulgadas. Se llaman de esta forma porque no tienen en cuenta a nadie más, no depende de la medida de otra unidad.

Los valores **relativas**, llevan este nombre porque depende de otra unidad de medida o elemento. Por ejemplo, porcentajes, vmx, em, entre otros.

Recuerda que podemos darle estilos a etiquetas HTML muy específicas indicando dónde se van a encontrar. Por ejemplo: si queremos darle estilos únicamente a la imagen que está dentro del header, podemos usar el selector css `header img { ... }`.

No olvides resolver el desafío: crear tu propio header con las etiquetas y estilos que más te gusten para compartirlo en la sección de discusiones.

# Displays en CSS

Todos los elementos en CSS son cuadrados o rectángulos y aparte de eso, estos elementos tienen un comportamiento que se define a través de la propiedad display. Los display más comunes y usados son: block, inline, inline-block, none, table, flex y grid. Veamos de qué se tratan:

![Display en CSS](src_platzi/Display-en-CSS-634bc14a-bdf5-4337-a67d-49fc74f92a60.jpg)

# Funciones de las propiedades CSS más usadas

![Funciones Propiedades CSS](src_platzi/Funciones-Propiedades-CSS_v2-51b32251-2a22-401f-82e5-ced2dbcb83c6.jpg)

# Posicionamiento en CSS 

El posicionamiento en CSS es una de las cosas más importantes, pues establece cómo van a estar ubicados nuestros elementos en la pantalla.

En CSS los elementos se posicionan utilizando las propiedades top (superior), bottom (inferior), left (izquierda) y right (derecha), pero sólo funcionarán si la propiedad position está establecida. Esto quiere decir que si quiero que mi elemento div esté completamente a la derecha, debo escribir en mi CSS lo siguiente:

`div { position: absolute: right: 0px; }`

La propiedad position tiene 7 valores diferentes: **relative**, **absolute**, **fixed**, **sticky**, **static**, **initial** e **inherit**. Veremos de qué se tratan:

![Posicionamiento en CSS](src_platzi/Posicionamiento-en-CSS-6477ec29-d5d2-44d0-b3f5-c2876e0ee739.jpg)

# ¿Qué son y para qué nos sirven las arquitecturas CSS?

Un objetivo claro es es que sean predecibles, reutilizable, escalable

Metodologias para mantener mejor el código.

## OOCSS 

css orientado a objetos, separa el diseño del contenido. Para reutilizar código. Ejemplo:

```html
<style>
.globalwidth {width100%;}
.header{}
.footer{}
</style>
<body>
<header class=”header globalwidth”>Header</header>
<footer class=”footer globalwidth”>Footer</footer>
```

## BEM: 

Block Element Modifier. Separa los bloques, los elementos y los modificadores. Ejemplo:

```html
<header class=”header”>
<button class=”header__button—red”>RED</button>
<button class=”header__button—yellow”>YELLOW</button>
```

## SMACSS: 

Arquitectura de css escalable y modular. La persona que creo esta metodología, realizó 5 pasos.

Dividir el css en componentes

* **Base:** estos componentes son los elementos que usamos en toda la aplicación.
* **Layout:** componentes que se utilizan en la página una sola vez, como el header o el footer.
* **Module:** estos módulos son componentes que usamos en la aplicación más de una vez.
* **State:** el estado serian los botones que cuando le hacemos clic cambian de color, seria como las acciones.
* **Theme:** son los temas, no todas las aplicaciones tienen temas. Pero cuando cambian los temas o los colores, se deben ver reflejados y los podamos separar de ese código.

## ITCSS: 

Triangulo invertido de CSS, esta metodología nos indica poder dividir todos los archivos de css en ciertas partes para que no se combinen entre si. Entonces debemos dividir el código en ajustes, herramientas, genérico, elementos, objetos, componentes y utilidades. Es una buena metodología para aplicar y evitar la especificidad, es decir, que hay elementos o clases que tienen mayor peso que otros.

## Atomic Design: 

Inspirado en la química, separo los elementos en átomos, moléculas, organismos, templates y páginas. Los átomos serian los elementos mas chiquitos como los botones, las moléculas serian un conjunto de esos botones, creciendo sucesivamente hasta formar organismos, templates y páginas completas.

# ¿Qué es un preprocesador, cuáles existen y cuáles son sus diferencias?

CSS es un lenguaje de hojas de estilo que nos permite crear sitios web agradables para el usuario, sin embargo, nuestros archivos de CSS suelen ser bastante extensos, lo que produce una demanda significativa de nuestro tiempo y puede generar un trabajo menos productivo.

Una de las cosas que puedes hacer para evitar tantas líneas de código es utilizar los preprocesadores de CSS, los cuales extienden las funcionalidades de CSS común, permitiéndonos tener variables, funciones, mixins, reutilización de código, flexibilidad en el desarrollo, etc.

Pero, ¿cómo es que funcionan los preprocesadores?

Un preprocesador se escribe con una sintaxis especial que nosotros le indicamos y debe compilarse a CSS para ser comprendido por el navegador. En sí lo que estamos haciendo es CSS pero con esteroides.

![Que es un procesador](src_platzi/Que-es-un-preprocesador-e37c3f15-9bc9-493a-ba6f-290600119061.jpg)

Esta sintaxis que te menciono depende de cada preprocesador. Los más conocidos y usados son: LESS, SASS y Stylus. ¿Cuál usar? En mi opinión personal, esta decisión depende más de tus gustos personales y de qué tan cómodo o cómoda te sientas con una sintaxis o con otra, sin embargo, es también importante que dialogues con tu equipo y evalúen con qué preprocesador quieren trabajar dependiendo de las necesidades del proyecto.

Aquí te comparto la documentación oficial de cada uno de ellos para que puedas evaluar sus diferencias en sintaxis:

* https://sass-lang.com/guide
* http://lesscss.org/
* http://stylus-lang.com/

Para aprender muy bien CSS y este tema de preprocesadores, es muy importante que practiques y seas constante. Comenzar algo siempre va a ser muy difícil, pero con paciencia y dedicación podrías llegar a dominar estos temas fácilmente.

Algo que me ayudó mucho cuando comencé, fue replicar páginas que me gustaban mucho (a modo de estudio, por supuesto). Comencé haciendo extensas líneas de CSS y quizás mis creaciones no eran las más bonitas, pero no me desmotivé, al contrario, seguí intentándolo y sumándole a esta práctica el tema de los preprocesadores, JavaScript, librerías, frameworks, etc.

Así que ánimo, practica bastante y por qué no, comparte tu conocimiento con la comunidad de Platzi o comunidades de desarrollo del lugar en donde vives.

¡Nos vemos en la siguiente clase!

# Instalación de SASS y configuración incial

Instalación de SASS con NPM:

`npm install -g sass`

Si usas Windows puedes usar el gestor de paquetes Chocolatey Package Manager e instalar SASS con el siguiente comando:

`choco install sass`

Si usas Mac puedes usar Homebrew para instalar SASS con el siguiente comando:

`brew install sass/sass/sass`

# FLEXBOX

[Flexbox](src_platzi/flexbox_96813fbc-64b2-496f-9e6e-f7e15161d563.pdf)